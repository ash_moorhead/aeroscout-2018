$(document).ready(function(){
	var originalValue='';
	var fields=['category', 'department', 'group'];
	autocompleteData={}; /* keep global */
	listOfFields=JSON.parse(localStorage.getItem('listOfFields'));
	$.each(fields, function(i, field) {
		autocompleteData[field]=[];
		$.each(listOfFields[field=='category' ? 'categories' : field+'s'], function (id, obj) { 
			obj.label=obj.name;
			obj.id=id;
			autocompleteData[field].push(obj);
		});
		$(".autocomplete."+field).autocomplete({
			source: autocompleteData[field],
			minLength: 1,
			//https://stackoverflow.com/questions/40782638/jquery-autocomplete-performance-going-down-with-each-search
			search: function(e,ui){
				$(this).data("ui-autocomplete").menu.bindings = $();
			},
			select: function( e, ui ) {
				// store new value when autocomplete selected
				if (ui.item.name!=originalValue) {
					originalValue=ui.item.name;
					var $t=$(e.target);
					var field=$t.attr('data-field');
					setValue($t, field, ui.item.id);
				}
			}
		})
	})

	$("table input").on('focusin', function(e) {
		// remember old value so we can tell if new value is different or same
		originalValue=e.target.value;
	});
	$("table input[data-field=name]").on('focusout keypress', function(e) {
		// store new value when leaving field or pressing enter
		if (e.type=='keypress' && e.keyCode!=13) return;
		var newValue=e.target.value;
		if (newValue!=originalValue) {
			var $t=$(e.target);
			setValue($t, 'name', newValue);
		}
	});
	$("table .autocomplete").on('focusout', function(e) {
		// when leaving autocomplete field *without selecting from menu* ...
		if (e.target.value=='' && originalValue!='') {
			// if new value is blank then store
			var $t=$(e.target);
			var field=$t.attr('data-field');
			setValue($t, field, 0);
		} else {
			// otherwise restore originalValue
			e.target.value=originalValue;
		}
	});
	$("#selectall").on('click', function(e) {
		// check or uncheck all checkboxes
		$(".select").prop('checked', e.target.checked);
	});
	var lastChecked = null;
	var $chkboxes=$("table .select");
	$chkboxes.on('click', function(e) {
		//https://stackoverflow.com/questions/659508/how-can-i-shift-select-multiple-checkboxes-like-gmail
		if (lastChecked && e.shiftKey) {
			var start = $chkboxes.index(this);
			var end = $chkboxes.index(lastChecked);
			$chkboxes.slice(Math.min(start,end), Math.max(start,end)+1).prop('checked', lastChecked.checked);
        }
        lastChecked = this;
		// make #selectall checkbox consisent with all checkboxes.
		$("#selectall").prop('checked', $(".select:not(:checked)").length==0);
	});
	$(".delete").on('click', function(e) {
		// delete an asset which is no longer unassigned
		var id=$(e.target).closest("tr").find("td[data-field=id]").text();
		getJSON('adminAjax.php?func=deleteUnassignedTag&id='+id, function(json) {
			$(e.target).closest("tr").remove();
		});
	});

	function setValue($target, field, value) {
		// send the new value to the server
		var id=$target.closest("tr").find("td[data-field=id]").text();
		var multi=false;
		if (!id) {
			multi=true;
			// get list of ids of all checked rows
			id=$(".select:checked").map(function(i, el) {
				return $(el).closest("tr").find("td[data-field=id]").text();
			}).get().join(',');
			if (!id) return;
		}
		getJSON('adminAjax.php?func=setUnassignedTagData&'+$.param({id: id, field: field, value: value}), function(json) {
			// TODO test if failed?
			if (multi) {
				$(".select:checked").each(function(i, el) {
					$target=$(el).closest("tr").find("input[data-field="+field+"]");
					$target.val(field=='name' ? value : (listOfFields[field=='category'?'categories':field+'s'][value]||{name:''}).name);
					showTick($target);
				});
			} else {
				showTick($target);
			}
		});
	}
	function showTick($target) {
		// animate the green 'tick' when successfully updated.
		$target.closest('div').addClass('good');
		setTimeout(function() {$target.closest('div').removeClass('good')}, 2000);
	}
});