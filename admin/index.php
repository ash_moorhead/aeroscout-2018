<?php
include('loggedin.php');	// check logged in
include_once("../api.php");

$unassignedTags=api::getUnassignedTagIds(true /* force refresh*/);
sort($unassignedTags);
$fakeTagData=api::fakeTagData();
$listOfFields=api::getListOfFields()['listOfFields'];

$unusedFakeTags=array();
foreach($fakeTagData as $tagId=>$tag) {
	if (!in_array($tagId, $unassignedTags)) $unusedFakeTags[$tagId]=$tag;
}

function data($id, $key) {
	global $fakeTagData;
	return (isset($fakeTagData[$id])) ? $fakeTagData[$id][$key] : '';
}

function data_name($id, $key) {
	global $listOfFields;
	$dataId=data($id, $key);
	if (!$dataId) {
		if ($key=='category') $dataId='0'; else return '';
	}
	$key = ($key=='category') ? 'categories' : $key.'s';
	return $listOfFields->{$key}->{$dataId}->name;
}

?><!doctype html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css">
<style>
table {
	border-collapse: collapse;
}
th, td {
	border:1px solid grey;
	padding:2px;
}
td {
	color:grey;
}
td div {
	display:relative;
}
td div.good::after {
	content:'✔';
	position:relative;
	display:inline-block;
	top:2px;
	line-height:0px;
	right:17px;
	margin-right:-17px;
	color:green;
    animation: fadeout 1s;
}
@keyframes fadeout {
    from { opacity: 0; }
    to   { opacity: 1; }
}
.delete {
	color:red;
	cursor:pointer;
}
</style>
</head>
<body>
<p>Welcome <?=$username?>!</p>
<p><a href="?action=logout">logout</a></p>
<p><a href="/ajax.php?func=wipeCache">Wipe Cache</a></p>
<p><a href="/">Back to App</a></p>
<h2>Unassigned Tags</h2>
<table>
<thead><tr>
	<th>MAC</th>
	<th><input type="checkbox" id="selectall"></th>
	<th>Name</th>
	<th>Category</th>
	<th>Department</th>
	<th>Group</th>
</tr></thead>
<tbody>
	<?php foreach($unassignedTags as $tagId) : ?>
		<tr>
			<td data-field='id'><?= $tagId ?></td>
			<td><input type="checkbox" class="select"></td>
			<td><div><input data-field='name' value="<?= data($tagId, 'name') ?>"></div></td>
			<td><div><input data-field='category' class="autocomplete category" value="<?= data_name($tagId, 'category') ?>"></div></td>
			<td><div><input data-field='department' class="autocomplete department" value="<?= data_name($tagId, 'department') ?>"></div></td>
			<td><div><input data-field='group' class="autocomplete group" value="<?= data_name($tagId, 'group') ?>"></div></td>
		</tr>
	<?php endforeach; ?>
	<?php foreach($unusedFakeTags as $tagId=>$tag) : ?>
		<tr>
			<td data-field='id'><?= $tagId ?></td>
			<td><input type="checkbox" class="select"> <span class="delete">✘</span></td>
			<td><div><input data-field='name' value="<?= data($tagId, 'name') ?>"></div></td>
			<td><div><input data-field='category' class="autocomplete category" value="<?= data_name($tagId, 'category') ?>"></div></td>
			<td><div><input data-field='department' class="autocomplete department" value="<?= data_name($tagId, 'department') ?>"></div></td>
			<td><div><input data-field='group' class="autocomplete group" value="<?= data_name($tagId, 'group') ?>"></div></td>
		</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan='2'>With all selected:</td>
		<td><div><input data-field='name' value=""></div></td>
		<td><div><input data-field='category' class="autocomplete category" value=""></div></td>
		<td><div><input data-field='department' class="autocomplete department" value=""></div></td>
		<td><div><input data-field='group' class="autocomplete group" value=""></div></td>
	</tr>
<tbody>
</table>
<?php if (count($unusedFakeTags)) :?>
	<p><span style="color:red">✘</span> indicates that the tag is no longer listed as unassigned (either not active or has been assigned to an asset), so can be deleted.</p>
<?php endif; ?>
<script src='/js/jquery-3.2.1.min.custom.js'></script>
<script src='/js/jquery-ui.min.js'></script>
<script src='<?= version_link("../js/polyfills.js")?>'></script>
<script src='<?= version_link("admin.js")?>'></script>
<script>
	fakeTagData=<?= json_encode($fakeTagData) ?>;
</script>
</body>
</html>