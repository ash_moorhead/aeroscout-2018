<?php
$users = array (
	"admin" => "bio",
	"CHCE" => "biomed"
);

$username = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : '';
$pass = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : '';

$validated = isset($users[$username]) && $pass==$users[$username];

if (!$validated) {
	header('WWW-Authenticate: Basic realm="AssetTrak"');
	header('HTTP/1.0 401 Unauthorized');
	die ("Not authorized");
}

if (isset($_GET['action']) && $_GET['action']=='logout') {
	header('HTTP/1.1 401 Unauthorized');
	echo <<<EOS
	<script>
	// try IE clear HTTP Authentication
    if (document.execCommand("ClearAuthenticationCache")) {
		window.location.href='/';
    } else if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
    	// chrome correctly forgets old login details
    	window.location.href='/';
    } else {
    	// firefox users need to be told to close all their windows...
    	window.location.href='/logout.html';
    }
    </script>
EOS;
	die('Admin access turned off');
}

// If arrives here, is a valid user.