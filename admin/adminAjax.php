<?php
include('loggedin.php');    // check logged in
include_once('../api.php');
$json=array();
$error=array();

// get ajax function name and remove it from the POST or GET array
$func = isset($_POST['func']) ? $_POST['func'] : $_GET['func'];
unset($_POST['func']);
unset($_GET['func']);

// get all parameters from query string and put into a single array
$data=array();
foreach ($_POST as $name=>$val) $data[$name]=$val;
foreach ($_GET as $name=>$val) $data[$name]=$val;
unset($data['timestamp']);

//****************************************

if ($func=="setUnassignedTagData") {
    $json['success']=api::setUnassignedTagData($data["id"], $data["field"], $data["value"]);
}
else if ($func=="deleteUnassignedTag") {
    $json['success']=api::deleteUnassignedTag($data["id"]);
}

header('Content-Type: application/json');
echo json_encode($error ? array('error'=>$error) : $json);
exit();