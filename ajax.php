<?php
include_once('api.php');
$json=array();
$error=array();

// get ajax function name and remove it from the POST or GET array
$func = isset($_POST['func']) ? $_POST['func'] : $_GET['func'];
unset($_POST['func']);
unset($_GET['func']);

// get all parameters from query string and put into a single array
$data=array();
foreach ($_POST as $name=>$val) $data[$name]=$val;
foreach ($_GET as $name=>$val) $data[$name]=$val;
unset($data['timestamp']);

//****************************************

if ($func=="getAssetsForMap") {
    $json['success']['assets']=api::findAssetsCurrentLocationByMapOrZone($data["map"], $data["zone"]);    
    // send the map and zone parameters back so the client knows if the data if for an old map when changing maps.
    $json['success']['map']=$data["map"];  
    $json['success']['zone']=$data["zone"];
}
else if ($func=="getAssetsFromSearchCriteria"){	 
    $json['success']=api::findAssetsBySearchCriteria($data); 
}
else if ($func=="getListOfFields"){
	$json['success']=api::getListOfFields();
}
else if ($func=="testAPI"){
	$json['success']=call_user_func_array(array('api','call'), $data['in']);
}
else if ($func=="wipeCache"){
	api::wipeCache(true /*verbose*/);
	exit();
}

header('Content-Type: application/json');
echo json_encode($error ? array('error'=>$error) : $json);
exit();