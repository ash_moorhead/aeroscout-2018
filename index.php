<?php
  include('api.php');
  $version = "0.12 11/04/2018";
  header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
  header("Pragma: no-cache"); // HTTP/1.0
  header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
?><!doctype html>
<html>
<head>
<title>Asset Tracker</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <link rel="shortcut icon" href="/resources/favicon2.ico">
  <link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css">
  <link rel="stylesheet" type="text/css" href="/css/MaterialIcons.css">
  <link rel="stylesheet" type="text/css" href="/css/dialog-polyfill.css">
  <link rel='stylesheet' type='text/css' href="/css/googlefont-Roboto.css">
  <link rel="stylesheet" type='text/css' href="/css/material.min.css">
  <link rel='stylesheet' type="text/css" href='<?= version_link("/css/style.css")?>'>
  <meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT"/>
  <meta http-equiv="pragma" content="no-cache" />
</head>
<body>
  <!-- Uses a header that contracts as the page scrolls down. -->
  <style>
  .demo-layout-waterfall .mdl-layout__header-row .mdl-navigation__link:last-of-type  {
    padding-right: 0;
  }
  </style>
  <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <header class="mdl-layout__header mdl-layout__header--waterfall">
      <!-- Top row, always visible -->
      <div class="mdl-layout__header-row header-height" style="padding: 0 16px;">
        <!-- Title -->
        <span class="mdl-layout-title" style="flex-shrink: 1;">Asset Tracker Application</span>
        <div class="mdl-layout-spacer"></div>

        <button id="clear-search-button" class="white-header-button mdl-button mdl-js-button mdl-js-ripple-effect">
          Clear Search
        </button>

        <!--   Filter     -->
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable
                    mdl-textfield--floating-label mdl-textfield--align-right">
          <label class="mdl-button mdl-js-button mdl-button--icon"
                 for="filter-input" id="filter-label">
            <i class="material-icons">&#xE152;</i>
          </label>
          <div class="mdl-textfield__expandable-holder">
            <input class="mdl-textfield__input" type="search" name="sample"
                   id="filter-input">
          </div>
        </div>

        <!--   Options     -->
        <button id="options-button" class="white-header-button mdl-button mdl-js-button">
          Options
        </button>

        <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu"
            for="options-button">
          <li id="set-user" class="mdl-menu__item">Set User</li>
          <li id="id-search" class="mdl-menu__item">Find specific asset</li>
          <li class="mdl-menu__item justify" style="pointer-events:none;" >
            <span style="font-weight:300;color: rgba(0,0,0,.87);">
              Show Fridges
            </span>
            <span style="pointer-events:all !important;">
              <label class="mdl-switch mdl-js-switch" for="show-fridges-switch">
                <span class="mdl-switch__label"></span>
                <input type="checkbox" id="show-fridges-switch" class="mdl-switch__input" />
              </label>
            </span>
          </li>
          <li class="mdl-menu__item justify" style="pointer-events:none;" >
            <span style="font-weight:300;color: rgba(0,0,0,.87);">
              Show Old Assets (>1hr)&nbsp;
            </span>
            <span style="pointer-events:all !important;">
              <label class="mdl-switch mdl-js-switch" for="show-old-switch">
                <span class="mdl-switch__label"></span>
                <input type="checkbox" id="show-old-switch" class="mdl-switch__input" />
              </label>
            </span>
          </li>
          <li id="admin" class="mdl-menu__item">Admin</li>
          <li id="about" class="mdl-menu__item">About</li>
        </ul>

      </div>

      <!-- Tabs -->
      <div class="mdl-layout__tab-bar mdl-js-ripple-effect" id = "tabBar">
        <a  href="#home" class="mdl-layout__tab is-active">Map</a>
        <a href="#find-my-assets" class="mdl-layout__tab">Find my Assets</a>
      </div>

    </header>

    <!-- Tab Content -->
    <main class="mdl-layout__content " id="main">
      <h3 id='spinner' class="mdl-typography--title">Loading data <div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div></h3>
      <section class="mdl-layout__tab-panel is-active" id="home">
          <div id='title' class="tab-title"> 
            <div>
              <span id="home-map-title" class="tab-title-map mdl-typography--display-1 mdl-color-text--grey-600">&nbsp;</span>
              <span id="home-zone-title" class="tab-title-zone mdl-typography--display-1 mdl-color-text--grey-600">&nbsp;</span>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ui-front">
              <input class="mdl-textfield__input" type="search" id="MapZone">
              <label class="mdl-textfield__label" for="MapZone">Change Map or Zone...</label>	
            </div>
          </div>
          <div id='map' class='map' class="display:none">
            <img src= '' class='map' id='mapImage'/>
            <svg></svg>
            <div id='assets' class='asset-container'></div>
          </div>
      </section>
      <section class="mdl-layout__tab-panel" id="find-my-assets">
        <div class="find-assets-page">
          <form id=search-form>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input class="mdl-textfield__input" type="search" id="departments">
              <label class="mdl-textfield__label" for="departments">Group or Department...</label>
               <span class="mdl-textfield__error">Invalid Department! Make sure to use autocomplete</span>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input class="mdl-textfield__input" type="search" id="categories">
              <label class="mdl-textfield__label" for="categories">Asset Categories...</label>
               <span class="mdl-textfield__error">Invalid Category! Make sure to use autocomplete</span>
            </div>
            <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="asset-search-button">
              Search
            </button>
          </form>
          <div id="progress" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
          <span id="noResults" class="validation">No results for those criteria! Try a new search or showing fridges.</span>
        </div>
      </section>

    </main>
  </div>

  <!-- Change/Set User Dialog -->
  <dialog id="dept-dialog" class="mdl-dialog">
      <div class="mdl-dialog__title even" style="font-size: 14px;color: rgba(0,0,0,.54);">
          <div style="display:inline-block;"> Current User:<br> 
              <span id="current-user" style="font-size:25px"></span>
          </div>
          <div class=dialog-icon> 
              <img src='http://mobileview/asset-manager-web/images/icons/Healthcare/pump_32X32.gif'>
              <span class='myDept'></span>
              <span>Your pumps</span>
          </div>
          <span style="display:inline-block; width:100%;"></span>
      </div>
      <div class="mdl-dialog__content dialog-content">
          <p>
              Select your department from the autocomplete menu shown when you start typing below.
              This will allow your department's assets to be distinguished from other departments in the map views. 
          </p>
          <p id="first-load">
              Come back and change this setting any time from the
              <span style="white-space:nowrap">"OPTIONS > Set User"</span> option in the top right of the screen.
          </p>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ui-front" style="width:100%;">
            <input class="mdl-textfield__input" type="search" id="dialog-Department">
            <label class="mdl-textfield__label" for="dialog-Department">Department...</label>
             <span class="mdl-textfield__error">Invalid Department! Use autocomplete.</span>
        </div>
      </div>
      <div class="mdl-dialog__actions button-container">
          <button id="save-button" type="button" class="mdl-button">Save</button>
          <button type="button" class="mdl-button close-button">Close</button>
          <button disabled id="clear-button" type="button" class="mdl-button">Clear</button>
      </div>
  </dialog>

  <!--       About Dialog -->
  <dialog id="about-dialog" class="mdl-dialog">
    <div class="mdl-dialog__title">
      <span class="mdl-typography--display-1 mdl-color-text--grey-600" >Asset Tracker</span>
      <div class="mdl-typography--title mdl-color-text--grey-600" style="font-size:14px;float:right; display:inline-block;">v<?php echo $version?></div>
    </div>
    <div class="mdl-dialog__content">
      <p>
        This application was created by the Medical Physics and Bioengineering Department.<br>
        For feedback or bugs please email:<br><a href="mailto:ash.moorhead@cdhb.health.nz">ash.moorhead@cdhb.health.nz</a>
      </p>
    </div>
    <div class="mdl-dialog__actions" style="text-align:left;display:block;">
      <button type="button" class="mdl-button close-button" style="float:right;">Close</button>
      <img src= "resources/MPBE_Logo.png" class='logo' id='mapImage'/>
    </div>
  </dialog>

  <!-- T No. Search Dialog   -->
  <dialog id="id-search-dialog" class="mdl-dialog">
    <h4 class="mdl-dialog__title mdl-typography--display-1 mdl-color-text--grey-600">Find specific asset</h4>
    <div class="mdl-dialog__content">
      <p>
      Start typing the <b>Name</b>, <b>ID</b> or <b>Serial Number</b> of the asset you are looking for.
      </p><p>
      You can select one of the suggested values, or click the "Search" button to execute your search.  
      </p>
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width: 100%;">
        <input class="mdl-textfield__input" type="search" id="application-id">
        <label class="mdl-textfield__label" for="application-id">Enter search text...</label>
        <span class="mdl-textfield__error">No matching asset exists.<b> Try showing fridges</b></span>
      </div>
    </div>
    <div class="mdl-dialog__actions" style="text-align:left;display:block;">
          <button type="button" id="id-search-button" class="mdl-button" style="float:right;">Search</button>
      <button type="button" class="mdl-button close-button" style="float:right;">Close</button>
    </div>
  </dialog>

  <!--  Tool Tips    -->
  <div class="mdl-tooltip" for="filter-label">
    Filter assets <br> by category
  </div>
      
  <script>
    listOfFieldsDate=<?php 
      $cacheFile=api::$cacheFolder.api::$listOfFieldsFile;
      //api::wipeCache(false); // delete cache every time, for testing.
      // delete cache if more than 1 week old
      if (file_exists($cacheFile) && ((time()-filemtime($cacheFile)) > 7*24*60*60)) api::wipeCache(false);
      // delete cache if we change its format.
      if (file_exists($cacheFile) && (filemtime($cacheFile)<1504224550)) api::wipeCache(false);
      // delete cache if we're messing around with the data format
      //api::wipeCache(false);
      echo file_exists($cacheFile) ? filemtime($cacheFile) : 0;
    ?>;
  </script>

  <script src='/js/jquery-3.2.1.min.custom.js'></script>
  <script src='/js/jquery-ui.min.js'></script>
  <script src="/js/material.min.js"></script>
  <script src='<?= version_link("/js/polyfills.js")?>'></script>
  <script src="/js/dialog-polyfill.js"></script>
  <script src='<?= version_link("/js/script.js")?>'></script>
</body>
</html>