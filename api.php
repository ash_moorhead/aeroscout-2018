<?php
set_error_handler("myErrorHandler");
ini_set('display_errors', 1);
error_reporting(E_ALL);

// confirm SOAP is installed
if (!extension_loaded('soap')) {
  echo 'ERROR: SOAP extension is not loaded!';
  die();
}

// set SOAP WSDL cache, http://php.net/manual/en/soap.configuration.php
ini_set('soap.wsdl_cache_enabled', WSDL_CACHE_BOTH); 
ini_set('soap.wsdl_cache_ttl', 1/*days*/ * 24/*hours*/ *60*60);
if (!file_exists(api::$cacheFolder)) mkdir(api::$cacheFolder);
ini_set('soap.wsdl_cache_dir', $_SERVER["DOCUMENT_ROOT"].'\\'.api::$cacheFolder);

class api {
	// file:///M:/Projects/AeroscoutMobileView/mv-ws/apidocs/index.html
	
	private static $url = "http://mobileview/asset-manager-web/services/";
	private static $username = "chhb";
	private static $password = "h3artb3at5!";
	public  static $cacheFolder=__DIR__.'/cache'; // __DIR__ makes relative to THIS file.
	public  static $listOfFieldsFile='/listOfFields.json';
	private static $useCache = true;
	// these lists are used to filter the data so less data is sent to the client.
	private static $assetFieldList= "point,tempCelsius,asset->applicationId,asset->primaryCategory->id,asset->primaryCategory->name,asset->primaryCategory->icon,dateCreated,asset->departments->DepartmentDTO->id,asset->name,asset->assetBusinessStatus->description,map->id,zones,tagDTO->deviceId";
	private static $mapFieldList = 'name,XScale,YScale,height,width,origin';
	private static $zoneFieldList = 'name,points,map->id,color,description';
	// used for assigning fake asset data to unassigned Tags
	private static $unassignedCategoryId = 0;
	private static $unassignedCategoryName = "Unassigned Tags";
	private static $fakeTagDataFile = __DIR__."/data.do.not.overwrite/fakeTagData.json";  // __DIR__ makes relative to THIS file.

	public static function isCached($path) {
		if (!self::$useCache) return false;
		return file_exists($path);
	}

	public static function wipeCache($verbose) {
		if (self::$cacheFolder=='') {
			if ($verbose) echo 'Cannot wipe cache from root folder';
			return;
		}
		if (!file_exists(self::$cacheFolder)) {
			if ($verbose) echo "Cache folder doesn't exist";
			return;
		}
		$files=glob(self::$cacheFolder."/*");
		$count=count($files);
		if ($count) {
			$p="Deleted {$count} files.";
			$div="Deleted files:<br><ul>";
			foreach($files as $file) {
				unlink($file);
				$div=$div."<li>$file</li>";
			}
			$div.="</ul>";
		} else {
			$p="Cache is empty";
			$div='';
		}
		if ($verbose) {
			echo <<<EOS
	<html><body>
	<p>$p</p>
	<p>Fetching new data.<span></span></p>
	<div style="color:#bbb">$div</div>
	<script src='js/jquery-3.2.1.min.custom.js'></script>
	<script>
		interval=setInterval(function() {
			$("span").text($("span").text()+".");
		}, 500);
		$.getJSON("ajax.php?func=getListOfFields", function(json) {
			$("span").html($("span").text()+" Done! <a href='/'>Go to app</a>");
			clearInterval(interval);
		});
	</script>
	</body></html>
EOS;
		}
	}

	private static function listByIdAndFilter($objects, $fields) {
		$newObj=array();
		foreach($objects as $object) {
			$newObj[$object->id]=api::filterFields($object, $fields);
		}
		return $newObj;
	}

	private static function filterFields($obj, $fields) {
		// takes a large object and returns an object with only the required fields
		// eg $fields = "name,id,map->id"
		if (!$fields) return $obj;
		$newObj=array();
		foreach(explode(',', $fields) as $field) {
			if ((strpos($field, '->') !== false)) {
				// if want a child field, need to go deeper into object until correct child is found
				$fields=explode('->', $field);
				$newParent=&$newObj;
				$value=$obj;
				// iterate down through the child objects
				for ($a=0; $a<count($fields)-1; $a++) {
					if (!array_key_exists($fields[$a], $newParent)) $newParent[$fields[$a]]=array();
 					if (!$value || !array_key_exists($fields[$a], $value) ){
 						$value = null;
 					} else {
 						$value =$value->{$fields[$a]};
 					}
					$newParent=&$newParent[$fields[$a]];
				}
 				$parent[$fields[$a]]= $value && array_key_exists($fields[$a], $value) ? $value->{$fields[$a]} : null;
				$newParent[$fields[$a]]= $value && array_key_exists($fields[$a], $value) ? $value->{$fields[$a]} : null;			
			} else {
				$newObj[$field]=$obj->$field;
			}
		};
		return $newObj;
	}

	public static function findAssetsCurrentLocationByMapOrZone($mapId, $zoneId) {
		// LocatorQueryResultDTO findAssetsCurrentLocationByMap(long mapId, QueryDTO query)
		if ($zoneId=='0') {
			$type='Map';
			$id=$mapId;
		} else {
			$type='Zone';
			$id=$zoneId;
		}
		$LocatorQueryResultDTO = self::call("Locator.findAssetsCurrentLocationBy$type", $id, self::QueryDTO(1, 1000));
		// this also gets all tags even if they are not allocated to an asset, but it doesn't return the asset data so use self::getUnassignedTagsLocation() instead
		//$LocatorQueryResultDTO = self::call("Locator.findTagsCurrentLocationBySpecificLocation", 1, $id, self::QueryDTO(1, 1000));

		if (property_exists($LocatorQueryResultDTO->out->locations, 'LocationDTO')) {
			$LocationDTO = $LocatorQueryResultDTO->out->locations->LocationDTO;
			$result = property_exists((object) $LocationDTO, 'id') 
				? array($LocationDTO)	// result is an single object
				: $LocationDTO;			// result is an array of objects
		} else {
			$result= array(); // result is empty
		}

		// add locations of any unassigned tags which are in the target map/zone
		$unassignedTags=self::getUnassignedTagsLocation();
		//phpConsole($unassignedTags);
		foreach($unassignedTags as $tag) {
			if ($type=='Zone') {
				if ($tag->zones) {
					if (property_exists((object) $tag->zones, 'id')) $tag->zones=array($tag->zones); // put single zone into an array for consistency with multiple zone result
					foreach($tag->zones as $zone) if ($zone->id==$zoneId) $result[] = $tag;
				}
			} else {
				if ($tag->map->id==$mapId) $result[] = $tag;
			}
		}

		return self::listByIdAndFilter($result, self::$assetFieldList);
	}

	public static function fakeTagData() {
		if (file_exists(self::$fakeTagDataFile)) return json_decode(file_get_contents(self::$fakeTagDataFile), JSON_OBJECT_AS_ARRAY);
		$data=array(
			"000CCC037D60" => array(
				"name" => "Peter's fake tag",
				"category" => 49, // mobile units 
				"department" => 156, // clinical engineering
				"group" => 5 // CE Test Tags
			),
			"78F882CF1AC4" => array(
				"name" => "Chris' fake tag",
				"category" => 49, // mobile units 
				"department" => 156, // clinical engineering
				"group" => 5 // CE Test Tags
			)
		 );
		if (!file_exists(dirname(self::$fakeTagDataFile))) mkdir(dirname(self::$fakeTagDataFile));
		file_put_contents(self::$fakeTagDataFile, json_encode($data));
		return $data;
	}

	private static function addFakeDataToUnassignedTag($tag) {
		$fakeTagData=self::fakeTagData();
		$fakeTag = array_key_exists($tag->tagDTO->deviceId, $fakeTagData)
			? $fakeTagData[$tag->tagDTO->deviceId]
			: array(
				'name' => 'Unassigned tag',
				'category' => self::$unassignedCategoryId,
				'department' => 0,
				'group' => 0
			);
		$tag->id = $tag->tagDTO->deviceId;
		$tag->asset = (object) array(
			'primaryCategory' => (object) array( 'id' => $fakeTag['category']),
			'name' => $fakeTag['name'].'-'.$tag->tagDTO->deviceId,
			'departments' => (object) array( 'DepartmentDTO' => (object) array( 'id' => $fakeTag['department'])),
			'groups' => (object) array( 'GroupDTO' => (object) array( 'id' => $fakeTag['group']))
		);
		return $tag;
	}

	private static function getUnassignedTagsLocation() {
		$unassignedTagIds = self::getUnassignedTagIds();
		if (count($unassignedTagIds)==0) return array();
		$LocationDTO = self::call("Locator.findTagsCurrentLocation", $unassignedTagIds);
		$unassignedTags=$LocationDTO->out->LocationDTO;
		if (property_exists((object) $unassignedTags, 'id')) $unassignedTags=array($unassignedTags); // put single tag into an array for consistency with multiple tag result
		foreach($unassignedTags as $tag) $tag=self::addFakeDataToUnassignedTag($tag); // this overwrites original array
		//phpConsole($unassignedTags); 
		return $unassignedTags;
	}

	public static function getUnassignedTagIds($forceUpdate=false) {
		$cacheFile=self::$cacheFolder."/unassignedTags.json";
		if (!$forceUpdate && self::isCached($cacheFile)) {
			$ids=json_decode(file_get_contents($cacheFile), JSON_OBJECT_AS_ARRAY);
		} else {
			// get list of unassigned tags by getting all tags and seeing which don't have an asset assigned.
			$LocatorQueryResultDTO = self::call("Locator.findAllTagsCurrentLocation", self::QueryDTO(1, 1000));
			$LocationDTO = $LocatorQueryResultDTO->out->locations->LocationDTO;
			$ids=array();
			foreach($LocationDTO as $tag) if ($tag->asset==null) $ids[]=$tag->tagDTO->deviceId;
			file_put_contents($cacheFile, json_encode($ids));
		}
		return $ids;
	}

	public static function setUnassignedTagData($tagId, $field, $value) {
		$fakeTagData=self::fakeTagData();
		$tagIds=explode(',', $tagId);
		$done=array();
		foreach($tagIds as $tagId) {
			$fakeTag = array_key_exists($tagId, $fakeTagData)
				? $fakeTagData[$tagId]
				: array(
					'name' => '',
					'category' => self::$unassignedCategoryId,
					'department' => 0,
					'group' => 0
				);
			$fakeTag[$field]=trim($value);
			if ($fakeTag['name']=='' && $fakeTag['category']*1==self::$unassignedCategoryId && $fakeTag['department']*1==0 && $fakeTag['group']*1==0) {
				unset($fakeTagData[$tagId]);
			} else {
				$fakeTagData[$tagId]=$fakeTag;
			}
			$done[]=$fakeTag;
		}
		file_put_contents(self::$fakeTagDataFile, json_encode($fakeTagData));
		return $done;
	}

	public static function deleteUnassignedTag($tagId) {
		$fakeTagData=self::fakeTagData();
		unset($fakeTagData[$tagId]);
		file_put_contents(self::$fakeTagDataFile, json_encode($fakeTagData));
		return true;
	}

	public static function getListOfFields() {
		$cacheFile=self::$cacheFolder.self::$listOfFieldsFile;
		if (self::isCached($cacheFile)) {
			return array(
				'listOfFields' => json_decode(file_get_contents($cacheFile)),
				'date' => filemtime($cacheFile)
			);
		}
		$maps=self::findAllMaps();
		$data=array(
			'Map' 			=> $maps,
			'Zone' 			=> self::findAllZones($maps),
			'departments'	=> self::findAll('Department', 'name,parent->id'),
			'groups' 		=> self::findAll('Group', 'name'),
			'categories'	=> self::findAllCategories()
		);
		file_put_contents($cacheFile, json_encode($data));
		return array(
			'listOfFields' => $data,
			'date' => filemtime($cacheFile)
		);
	}

	private static function findAllMaps() {
		$MapDTO = self::call('Area.findAllMaps');
		$maps = self::listByIdAndFilter($MapDTO->out->MapDTO, self::$mapFieldList);
		return $maps;
	}

	private static function findAllZones($maps) {
		$zonesById=array();
		foreach($maps as $mapId=>$map) {
			$ZoneDTO = self::call('Area.findZonesByMapId', $mapId);
			if ($ZoneDTO->out != new stdClass()){
				if(isset($ZoneDTO->out->ZoneDTO->name)) {
					// result is a single zone
					$zonesById[$ZoneDTO->out->ZoneDTO->id] = self::filterFields($ZoneDTO->out->ZoneDTO, self::$zoneFieldList);
				} else {
					// result is an array of zones
					foreach($ZoneDTO->out->ZoneDTO as $zone){
						$zonesById[$zone->id] = self::filterFields($zone, self::$zoneFieldList);
					}
				}
			}
		}
		return $zonesById;
	}

	private static function findCategoryChildren($parentId, $category, &$output_array /*passed by reference*/) {
		// convert nested category objects into flat array indexed by category id.
		$children = array();
		// get a list of this category's direct children
		if(!(!property_exists($category, 'children') || !property_exists($category->children, 'CategoryDTO'))){
			if(gettype($category->children->CategoryDTO) === 'array'){
				// more than one child, so add each to the list of children
				foreach($category->children->CategoryDTO as $child){
					$children[] = array(
						'id' => $child->id,
						'name' => $child->name
					);
				}
			} else {
				// only one child
				$children[] = array(
						'id' => $category->children->CategoryDTO->id,
						'name' => $category->children->CategoryDTO->name
					);
			}
		}
		// store this category in the output array
		$output_array[$category->id]=array(
			'name' => $category->name,
			'parentId' => $parentId,
			'icon' => $category->icon,
			'hasChildren' => !empty($children),
			'children' => $children
		);

		if (empty($children)) return;
		// now recurse over the children to add them to the output and find their children
		if (is_object($category->children->CategoryDTO)) {
			// only one child
			self::findCategoryChildren($category->id, $category->children->CategoryDTO, $output_array);
		} else {
			// multiple children
			foreach($category->children->CategoryDTO as $child) {
				self::findCategoryChildren($category->id, $child, $output_array);
			};
		}
	}

	private static function findAllCategories() {
		$CategoryDTO = self::call('Category.findRootCategoryTree');
		$array=array();
		// add fake category for unassigned tags
		$array[self::$unassignedCategoryId]=array(
			'name' => self::$unassignedCategoryName,
			'parentId' => null,
			'icon' => "/images/x_tag_on.gif",
			'hasChildren' => false,
			'children' => array()
		);
		self::findCategoryChildren(null, $CategoryDTO->out, $array);
		return $array;
	}

	private static function findAll($field, $filter){
		$DTO = self::call("$field.findAll{$field}s");
		$typeDTO = sprintf("%sDTO", $field);
		$result = self::listByIdAndFilter($DTO->out->$typeDTO, $filter);
		return $result;
	}

	private static function getZoneImage($zoneId) {
		//  'Image.retrieveZoneImage' doesn't seem to work.  Have to resort to cropping image ourselves.
		$zoneImageFile=self::$cacheFolder."/zone_$zoneId.jpg";

		if (!self::isCached($zoneImageFile)) {
			$ZoneDTO = self::call('Area.findPopulatedZoneById', $zoneId);
			$map = $ZoneDTO->out->map;
			$points = $ZoneDTO->out->points->PointDTO;
			
			// get bounding rectangle of zone from it's list of points
			$minx=$maxx=$points[0]->x;
			$miny=$maxy=$points[0]->y;
			foreach ($points as $point) {
				if ($point->x < $minx) $minx=$point->x;
				if ($point->y < $miny) $miny=$point->y;
				if ($point->x > $maxx) $maxx=$point->x;
				if ($point->y > $maxy) $maxy=$point->y;
			}
			// load the map image
			$image=imagecreatefromjpeg(self::getImage('map', $map->id));
			// calculate crop coordinates
			$crop=array(
				'x' => intval($minx/$map->XScale + $map->origin->x),
				'y' => intval(-$maxy/$map->YScale + $map->origin->y),
				'width' => intval(($maxx-$minx)/$map->XScale),
				'height' => intval(($maxy-$miny)/$map->YScale)
			);
			// make sure crop isn't outside image
			if ($crop['x']<0) $crop['x']=0;
			if ($crop['x']+$crop['width'] > $map->width) $crop['width'] = $map->width-$crop['x'];
			if ($crop['y']<0) $crop['y']=0;
			if ($crop['y']+$crop['height'] > $map->width) $crop['height'] = $map->width-$crop['y'];
			// crop and save the imgae
			$image=imagecrop($image, $crop);
			imagejpeg($image, $zoneImageFile);
			imagedestroy($image);
		}

		return $zoneImageFile;
	}
	
	// needs to be public, called from image.php when file doesn't already exist
	public static function getImage($type, $id) {
		if ($type=='zone') return self::getZoneImage($id);
		$imageFile=self::$cacheFolder."/{$type}_$id.jpg";

		if (!self::isCached($imageFile)) {
			$ImageResizeResultDTO = self::call('Image.retrieve'.ucwords($type).'Image', $id, 0, 0);
			if (!property_exists((object) $ImageResizeResultDTO->out, 'image')) {
				header("HTTP/1.0 404 Not Found");
				die();
			};
			$ImageDTO = $ImageResizeResultDTO->out->image->data;
			file_put_contents($imageFile, $ImageDTO);
		}
		
		return $imageFile;
	}

	public static function findAssetsBySearchCriteria($criteria){
		if(!$criteria) return; // if search criteria is null here all assets will be returned

		$AssetEntitiesLayout = self::AssetEntitiesLayout(true, true, true, false, false, false);
		
		$LocatorQueryResultDTO = self::call('Locator.findAssetsCurrentLocationByLocatorSearchCriteria', $criteria, $AssetEntitiesLayout, self::QueryDTO(1, 1000));
		$result = ($LocatorQueryResultDTO->out == new stdClass() || $LocatorQueryResultDTO->out->locations == new stdClass()) 
			? null
			: (property_exists((object) $LocatorQueryResultDTO->out->locations->LocationDTO, 'id') 
				? array($LocatorQueryResultDTO->out->locations->LocationDTO)	// result is a single object
				: $LocatorQueryResultDTO->out->locations->LocationDTO			// result is an array of objects	
			  );
		
		// add any unassigned tags which match the search
		$unassignedTags=self::getUnassignedTagsLocation();
		foreach($unassignedTags as $id=>$tag) {
			$match = true; // search is AND, so assume true and set to false if any match fails.
			if (array_key_exists('categories', $criteria) && !in_array($tag->asset->primaryCategory->id, $criteria['categories'])) $match=false;
			if ($match && array_key_exists('departments', $criteria) && !in_array($tag->asset->departments->DepartmentDTO->id, $criteria['departments'])) $match=false;
			if ($match && array_key_exists('groups', $criteria) && !in_array($tag->asset->groups->GroupDTO->id, $criteria['groups'])) $match=false;
			if ($match && array_key_exists('freeText', $criteria) && stripos($tag->asset->name, $criteria['freeText'])===false && stripos($tag->id, $criteria['freeText'])===false) $match=false;
			if ($match) {
				if (!$result) $result=array();
				$result[] = $tag;
			}
		}
		return ($result == null) ? null : self::listByIdAndFilter($result, self::$assetFieldList);
	}

	public static function call() { // first arg = "APIname.function", rest are parameters for the function
		$args=array();
     	$numargs = func_num_args();
		for ($a=1; $a<$numargs; $a++) $args['in'.($a-1)] = func_get_arg($a);

		$fn=explode('.', func_get_arg(0));

		try {
			$result = self::getClient($fn[0])->__soapCall($fn[1], array($args));
		} catch (SoapFault $e){
		 	phpConsole($e);
		 	die();
		}
		
		return $result;
	}
	
	private static function getClient($client) {
		// create soapClient from wsdl file, and store it in case it is used again in this thread.
		// note that wsdl files are also cached in PHP memory and on disk, so creating a new client isn't too expensive.
		static $soapClients = array();
		if (isset($soapClients[$client])) return $soapClients[$client];

		$soapClients[$client] = new SoapClient(self::$url.$client."APIService?wsdl",
		 	array('login'    => self::$username,
				  'password' => self::$password)
		);
		return $soapClients[$client];
	}

	private static function QueryDTO($pageNo, $maxResults) {
	 	return array(
			"pageNo"   => $pageNo,
			"pageSize" => $maxResults
		);
	}

	private static function AssetEntitiesLayout($fetchCatergories, $fetchCustomProperties, $fetchDepartments, $fetchGroups, $fetchImage, $fetchTags) {
		return array(
			"fetchCategoriess"   => $fetchCatergories,
			"fetchCustomProperties" => $fetchCustomProperties,
			"fetchDepartments"   => $fetchDepartments,
			"fetchGroups" => $fetchGroups,
			"fetchImage"   => $fetchImage,
			"fetchTags" => $fetchTags,
		);
	}
}

function phpConsole($msg) {
	global $json;
	if (func_num_args()== 1)
		$json['console'][]=$msg;
	else
		$json['console'][$msg]=func_get_arg(1);
}

function phpError($msg) {
	global $json;
	if (func_num_args()== 1)
		$json['error'][]=$msg;
	else
		$json['error'][$msg]=func_get_arg(1);
}

function myErrorHandler($errno, $errstr, $errfile, $errline) {
	global $json;
	if (gettype($json)=='NULL') {;
		echo "$errstr in $errfile at line $errline";
		exit();
	}
	header('Content-Type: application/json');
	echo json_encode(array('error'=>"$errstr in $errfile at line $errline"));
	exit();
}

function version_link($path) {
	/* 	include file timestamp in filename so that whenever file changes the browser's cache is automatically ignored.
		converts 'js/script.js' to 'js/script.v123456.js'
		requires URL redirection on the server to igore the .v123456 part, i.e.

		apache's .htaccess:
			RewriteEngine On
			# Allow versioning of css and script files to prevent cached file being used - style.v3.css => style.css
			RewriteRule ^(.*)\.v\d*\.(.*) $1.$2 [L]

		IIS web.config:
			<rule name="versioning css and js files" stopProcessing="true">
				<match url="^(.*)\.v\d*\.(.*)" />
				<conditions>
					<add input="{REQUEST_FILENAME}" matchType="IsFile" negate="true" />
				</conditions>
				<action type="Rewrite" url="{R:1}.{R:2}" />
			</rule>
	*/
	// regex looks for '.'.js or .css at end of string and replaces it with timestamp + extension.
	return $path; // to help during development in Chrome DevTools
	return preg_replace('/(\.\w{1,3})$/i','.v'.@filemtime($_SERVER['DOCUMENT_ROOT'].'/'.$path).'$1',$path,1);
}