<?php
/*
	If a file doesn't exist locally, get it from the api and save it locally, then serve it.

	IIS web.config:

		<rule name="Forward images to php if they don't exist" stopProcessing="true">
			<match url="^cache/(.*)_(.*)\.jpg$" />
			<conditions>
				<add input="{REQUEST_FILENAME}" matchType="IsFile" negate="true" />
			</conditions>
			<action type="Rewrite" url="image.php?type={R:1}&amp;id={R:2}" />
		</rule>

	Apache .htaccess:
		RewriteEngine On

		#Igore if 'real' file (images, stylesheets etc.)
		RewriteCond %{REQUEST_FILENAME} -s [OR]
		RewriteCond %{REQUEST_FILENAME} -l [OR]
		RewriteCond %{REQUEST_FILENAME} -d
		RewriteRule ^.*$ - [NC,L]

		# Forward images to php if they don't exist
		RewriteRule ^cache/(.*)_(.*)\.jpg$ image.php?type=$1&id=$2 [L]
*/
include_once('api.php');

$file=api::getImage($_GET['type'], $_GET['id']);

header("Content-Type: image/jpeg");
echo file_get_contents($file);
