/****** OUR OWN WRAPPER FUNCTION ********************/

function getJSON(url, successCallback, failCallback) {
	/* this is our own wrapper for jquery's $.getJSON which handles our own console and error logs from PHP */
 	//console.log('getJSON', url);
 	// add timestamp to url to avoid over-aggressive caching in IE
	$.getJSON(url+"&timestamp="+Date.now(), function(json) {
		if (json.console) $.each(json.console, function(i, o) {console.log('PHP:', i, o); });
		if (json.error) console.warn('PHP error:', json.error);
		//if (json.success) 
		successCallback(json.success);
	}).fail(failCallback || handleHTTPFail);
}

function handleHTTPFail(jqxhr, textStatus, error) {
	console.log("Request Failed: " + textStatus + ", " + jqxhr.responseText +", " +error);
	console.log(jqxhr);
}

function testAPI(apiName) {
	var paramString = [];

    for (var i = 0; i < arguments.length; i++) {
    	paramString+="&in[]="+arguments[i];
	}
	getJSON("ajax.php?func=testAPI&"+paramString , function(json){
		console.log(json);
	});
}

/********** JQUERY POLYFILLS *****************************/

// https://stackoverflow.com/questions/21290775/jquery-el-triggerchange-doesnt-fire-native-listeners
(function($) {
    $.fn.triggerNative = function(eventName) {
        return this.each(function() {
            var el = $(this).get(0);
            triggerNativeEvent(el, eventName);
        });
    };

    function triggerNativeEvent(el, eventName){
      if (el.fireEvent) { // < IE9
        (el.fireEvent('on' + eventName));
      } else {
        var evt = document.createEvent('Events');
        evt.initEvent(eventName, true, false);
        el.dispatchEvent(evt);
      }
	}
}(jQuery)); 

// jquery.deparam.min.js
(function(h){h.deparam=function(i,j){var d={},k={"true":!0,"false":!1,"null":null};h.each(i.replace(/\+/g," ").split("&"),function(i,l){var m;var a=l.split("="),c=decodeURIComponent(a[0]),g=d,f=0,b=c.split("]["),e=b.length-1;/\[/.test(b[0])&&/\]$/.test(b[e])?(b[e]=b[e].replace(/\]$/,""),b=b.shift().split("[").concat(b),e=b.length-1):e=0;if(2===a.length)if(a=decodeURIComponent(a[1]),j&&(a=a&&!isNaN(a)?+a:"undefined"===a?void 0:void 0!==k[a]?k[a]:a),e)for(;f<=e;f++)c=""===b[f]?g.length:b[f],m=g[c]=
f<e?g[c]||(b[f+1]&&isNaN(b[f+1])?{}:[]):a,g=m;else h.isArray(d[c])?d[c].push(a):d[c]=void 0!==d[c]?[d[c],a]:a;else c&&(d[c]=j?void 0:"")});return d}})(jQuery);


/********** JQUERY UI AUTOCOMPLETE POLYFILLS *****************************/

// https://jqueryui.com/autocomplete/#categories
function attachCategoryAutoComplete(elementId, availableFields){
	$.widget( "custom.catcomplete", $.ui.autocomplete, {
	  _create: function() {
		this._super();
		this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
	  },
	  _renderMenu: function( ul, items ) {
		var that = this,
		  currentCategory = "";

		$.each( items, function( index, item ) {
		  var li;
		  if ( item.category != currentCategory ) {
			ul.append( "<li class='ui-autocomplete-category'>" + (item.category.substring(0, 1).toUpperCase() + item.category.substring(1)) + "</li>" );
			currentCategory = item.category;
		  }
		  li = that._renderItemData( ul, item );
		  if ( item.category ) {
			li.attr( "aria-label", item.category + " : " + item.label );
		  }
		});
	  }
	});

	$( "#"+elementId ).catcomplete({
	  delay: 0,
	  focus: function(){
				return false;
			},
		select: function (event, ui){
			  $('#'+elementId).data(ui.item.value+"id", ui.item.id);
			  $('#'+elementId).data(ui.item.value+"category", ui.item.category)
		  },
		  //https://stackoverflow.com/questions/40782638/jquery-autocomplete-performance-going-down-with-each-search
		  search: function(e,ui){
			 $(this).data("customCatcomplete").menu.bindings = $();
			},
	  source: availableFields
	});
}

// https://jqueryui.com/autocomplete/#multiple
function attachMultiAutoComplete(elementId, availableFields){
	$("#" + elementId)
		// don't navigate away from the field on tab when selecting an item
		.on( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB &&
			$( this ).autocomplete( "instance" ).menu.active ) {
		  event.preventDefault();
		}
		})
		.autocomplete({
		minLength: 1,
		source: function( request, response ) {
				  // delegate back to autocomplete, but extract the last term
				  response( $.ui.autocomplete.filter(
					availableFields, extractLast( request.term ) ) );
				},
		focus: function(){
					return false;
				},
		//https://stackoverflow.com/questions/40782638/jquery-autocomplete-performance-going-down-with-each-search
		search: function(e,ui){
				 $(this).data("ui-autocomplete").menu.bindings = $();
				},
		select: function( event, ui ) {
				  var terms = split( this.value );
				  // remove the current input
				  terms.pop();
				  // add the selected item
				  terms.push( ui.item.value );
				  // add placeholder to get the comma-and-space at the end
				  terms.push( "" );
				  this.value = terms.join( ", " );
				  $(this).data(terms[terms.length-2]+"id", ui.item.id);
				  $(this).data(terms[terms.length-2]+"category", ui.item.category)
				  return false;
				}
		});

	function split( val ) {
	  return val.split( /,\s*/ );
	}

	function extractLast( term ) {
	  return split( term ).pop();
	}
}

/********* NATIVE POLYFILLS *****************************/

// https://stackoverflow.com/a/28269330
// repeatedly waits until a condition is true before calling a function
var waitUntilTrue = function(condFunc, readyFunc, checkInterval) {
	var checkFunc = function() {
		condFunc() ? readyFunc() :  setTimeout(checkFunc, checkInterval || 100);
	};
	checkFunc();
};

// https://tc39.github.io/ecma262/#sec-array.prototype.includes
if (!Array.prototype.includes) {
  Object.defineProperty(Array.prototype, 'includes', {
    value: function(searchElement, fromIndex) {

      // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If len is 0, return false.
      if (len === 0) {
        return false;
      }

      // 4. Let n be ? ToInteger(fromIndex).
      //    (If fromIndex is undefined, this step produces the value 0.)
      var n = fromIndex | 0;

      // 5. If n ≥ 0, then
      //  a. Let k be n.
      // 6. Else n < 0,
      //  a. Let k be len + n.
      //  b. If k < 0, let k be 0.
      var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

      function sameValueZero(x, y) {
        return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
      }

      // 7. Repeat, while k < len
      while (k < len) {
        // a. Let elementK be the result of ? Get(O, ! ToString(k)).
        // b. If SameValueZero(searchElement, elementK) is true, return true.
        // c. Increase k by 1. 
        if (sameValueZero(o[k], searchElement)) {
          return true;
        }
        k++;
      }

      // 8. Return false
      return false;
    }
  });
}


/********* MATERIAL DESIGN LIGHT HACKS *****************************/

// hack MDL to remove animation from menu, since it's really slow in IE.
// original version fd21836 on 25 Feb: https://github.com/google/material-design-lite/blob/mdl-1.x/src/menu/menu.js
if (typeof MaterialMenu!='undefined') {
  /**
   * Displays the menu.
   *
   * @public
   */
  MaterialMenu.prototype.show = function(evt) {
    if (this.element_ && this.container_ && this.outline_) {
      // Measure the inner element.
      var height = this.element_.getBoundingClientRect().height;
      var width = this.element_.getBoundingClientRect().width;

      // Apply the inner element's size to the container and outline.
      this.container_.style.width = width + 'px';
      this.container_.style.height = height + 'px';
      this.outline_.style.width = width + 'px';
      this.outline_.style.height = height + 'px';

      this.container_.classList.add(this.CssClasses_.IS_VISIBLE);
      this.applyClip_(height, width);
      this.element_.style.clip = 'rect(0 ' + width + 'px ' + height + 'px 0)';
/*
      var transitionDuration = this.Constant_.TRANSITION_DURATION_SECONDS *
          this.Constant_.TRANSITION_DURATION_FRACTION;

      // Calculate transition delays for individual menu items, so that they fade
      // in one at a time.
      var items = this.element_.querySelectorAll('.' + this.CssClasses_.ITEM);
      for (var i = 0; i < items.length; i++) {
        var itemDelay = null;
        if (this.element_.classList.contains(this.CssClasses_.TOP_LEFT) ||
            this.element_.classList.contains(this.CssClasses_.TOP_RIGHT)) {
          itemDelay = ((height - items[i].offsetTop - items[i].offsetHeight) /
              height * transitionDuration) + 's';
        } else {
          itemDelay = (items[i].offsetTop / height * transitionDuration) + 's';
        }
        items[i].style.transitionDelay = itemDelay;
      }

      // Apply the initial clip to the text before we start animating.
      this.applyClip_(height, width);

      // Wait for the next frame, turn on animation, and apply the final clip.
      // Also make it visible. This triggers the transitions.
      window.requestAnimationFrame(function() {
        this.element_.classList.add(this.CssClasses_.IS_ANIMATING);
        this.element_.style.clip = 'rect(0 ' + width + 'px ' + height + 'px 0)';
        this.container_.classList.add(this.CssClasses_.IS_VISIBLE);
      }.bind(this));

      // Clean up after the animation is complete.
      this.addAnimationEndListener_();
*/
      // Add a click listener to the document, to close the menu.
      var callback = function(e) {
        // Check to see if the document is processing the same event that
        // displayed the menu in the first place. If so, do nothing.
        // Also check to see if the menu is in the process of closing itself, and
        // do nothing in that case.
        // Also check if the clicked element is a menu item
        // if so, do nothing.
        if (e !== evt && !this.closing_ && e.target.parentNode !== this.element_) {
          document.removeEventListener('click', callback);
          this.hide();
        } else if (this.closing_) {
          document.removeEventListener('click', callback);
        }
      }.bind(this);
      document.addEventListener('click', callback);
    }
  };
  MaterialMenu.prototype['show'] = MaterialMenu.prototype.show;

  /**
   * Hides the menu.
   *
   * @public
   */
  MaterialMenu.prototype.hide = function() {
    if (this.element_ && this.container_ && this.outline_) {

      this.container_.classList.remove(this.CssClasses_.IS_VISIBLE);
      
      /*
      var items = this.element_.querySelectorAll('.' + this.CssClasses_.ITEM);

      // Remove all transition delays; menu items fade out concurrently.
      for (var i = 0; i < items.length; i++) {
        items[i].style.removeProperty('transition-delay');
      }

      // Measure the inner element.
      var rect = this.element_.getBoundingClientRect();
      var height = rect.height;
      var width = rect.width;

      // Turn on animation, and apply the final clip. Also make invisible.
      // This triggers the transitions.
      this.element_.classList.add(this.CssClasses_.IS_ANIMATING);
      this.applyClip_(height, width);
      this.container_.classList.remove(this.CssClasses_.IS_VISIBLE);

      // Clean up after the animation is complete.
      this.addAnimationEndListener_();
      */
    }
  };
  MaterialMenu.prototype['hide'] = MaterialMenu.prototype.hide;
}