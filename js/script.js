currentState = null;
showFridges = null;
showOld = null;
catImgPath='http://mobileview/asset-manager-web';
formattedAssets = null;
homeAssets = null;
temperatureMonitoringCategories = [3]; // used for hiding fridges & freezers, will be filled with all child categories
userDepartment = null;

$(document).ready(function(){
	// get listOfFields which contains maps, zones, departments, groups & categories
	// either get it from localStorage, or server if newer
	// note listOfFieldsDate is defined in index.php by the server.
	if (listOfFieldsDate==0 || listOfFieldsDate>localStorage.getItem('listOfFieldsDate')) {
		// server's cache of listOfFields has been wiped, so we should ignore our own cache as well
		// OR server's listOfFields if newer than our localCopy, so fetch the new one.
		getListOfFields();
	} else {
		listOfFields=localStorage.getItem('listOfFields');
		if (listOfFields) {
			listOfFields=JSON.parse(listOfFields);
			dataLoaded();
		} else {
			getListOfFields();
		}
	}	
});

/************** Data Initialising/Maintaining ******************/

function getListOfFields() {
	$("section.is-active, .mdl-layout__tab-bar").hide();
	$("#clear-search-button").hide();
	getJSON("ajax.php?func=getListOfFields", function(json) {
		listOfFields=json.listOfFields;
		localStorage.setItem('listOfFields', JSON.stringify(listOfFields));
		localStorage.setItem('listOfFieldsDate', json.date);
		dataLoaded();
	});	
}

function dataLoaded() {
	$("#spinner").remove();
	//using .show() overrides the is-active control
  	$("section.is-active, .mdl-layout__tab-bar").removeAttr('style');

	// when reloaded from a blank bookmark or app shortcut, and we have a previous location stored, go to the previous location.
	var lastLocation = localStorage.getItem('queryString');
	if (!window.location.search && lastLocation) {
		history.pushState({}, 'page', lastLocation);
	}

	currentState = getCurrentStateFromURL();

	// wait until mdl-layout is upgraded, see polyfills.js for waitUntilTrue()
	waitUntilTrue(function() {return document.documentElement.classList.contains('mdl-js')}, function(){
		// if no hash on the URL, automatically select first tab
		populateSearchFields(!window.location.hash);	// this also calls getAssetsBySearchCriteria()
	});

	showFridges = JSON.parse(localStorage.getItem('showFridges'));
	if (showFridges) $("#show-fridges-switch").prop('checked', true).parent().addClass('is-checked');
	
	showOld = JSON.parse(localStorage.getItem('showOld'));
	if (showOld) $("#show-old-switch").prop('checked', true).parent().addClass('is-checked');
	
	// check if userDepartment has been set by the URL
	userDepartment = getQueryElement("userDepartment");
	if (userDepartment) {
		localStorage.setItem('userDepartment', userDepartment);
	} else {
		userDepartment = JSON.parse(localStorage.getItem('userDepartment'));
	}
	if (userDepartment) {
		$("#set-user").text("Change User");
		$("#clear-button")[0].disabled = false;
		$("#current-user").text(""+listOfFields.departments[userDepartment].name);
	}else {
		$("#current-user").parent().hide();
	}
	
	changeMap(currentState.mapId, currentState.zoneId, true);	// this also initiates the fetchAssets, flag indicates state is pushed to history
	processMapsAndZones();			// calculate boundaries and create autocomplete
	processDepartmentsAndGroups();	// create autocomplete
	processCategories();			// create autocomplete

 	setInterval(function() {
 		fetchAssets();
		getAssetsBySearchCriteria();
	}, 180/*seconds*/ *1000);
	//}, 5/*seconds*/ *1000); console.error('set interval back to 3 min'); // for testing


	// select correct tab
	var hash = window.location.hash.substr(1);
	if(hash && !$("#"+hash+".is-active").length) selectTab(hash);	

	// set up event listeners
	$(window).on('resize', resizeMap);
	$('#asset-search-button').on('click', assetSearch);
	$('#filter-input').on('keyup search input', filterAssets);
	$("#clear-search-button").on('click', clearSearch);
	$('body').on('click','.has-zone', function() {
		changeMap(this.dataset.mapid, 0, false);
		selectTab("home"); // also records state change
	});
	$("#tabBar").on('click', 'a', hashChange);
	$("#show-fridges-switch").on('click', function() {
		showFridges = this.checked;
		localStorage.setItem('showFridges', showFridges);
		redrawAllAssets();
	});
	$("#show-old-switch").on('click', function() {
		showOld = this.checked;
		localStorage.setItem('showOld', showOld);
 		fetchAssets();
		getAssetsBySearchCriteria();
	});
	
	// set up modal event listeners
	var deptDialog = document.querySelector('#dept-dialog');
	var aboutDialog = document.querySelector('#about-dialog');
	var idSearchDialog = document.querySelector('#id-search-dialog');
	$(document).click(function(event) {
		if(event.target === deptDialog) deptDialog.close();
		if(event.target === aboutDialog) aboutDialog.close();
		if(event.target === idSearchDialog) idSearchDialog.close();
	});
	if (!deptDialog.showModal) {
		dialogPolyfill.registerDialog(deptDialog);
		dialogPolyfill.registerDialog(aboutDialog);
		dialogPolyfill.registerDialog(idSearchDialog);
	}	
	$("#set-user").on('click', function () {
		$("#first-load").remove(); 
		deptDialog.showModal();
		$('#dialog-Department').val("").autocomplete( "close" ).focus().parent().removeClass('is-invalid'); // focus() only required by IE.
	});
	$("#id-search").on('click', function(){
		idSearchDialog.showModal();
		$("#application-id").val("").autocomplete( "close" ).focus().parent().removeClass('is-invalid'); // focus() only required by IE.
	});
	$("#about").on('click', function(){
		aboutDialog.showModal();
	});
	$("#admin").on('click', function(){
		window.location='admin';
	});
	$('#dialog-Department').on('keyup', function(e){
		if(e.keyCode == 13) saveDepartment();
	});
	$("#save-button").on('click', saveDepartment);
	$("#clear-button").on('click', clearDepartment);
	$("button.close-button").on('click', function(event) {
		$(event.target).closest('.mdl-dialog')[0].close();
	});
	$("#id-search-button").on('click', idSearch);
	// Register autocomplete handler
	$("#application-id").on('keyup', function(e){
		if(e.keyCode == 13) idSearch();
	}).autocomplete({
		source: function (request, response) {
			var idString = $.param({freeText: request.term});
			getJSON("ajax.php?func=getAssetsFromSearchCriteria&"+ idString , function(locationDTO){
				removeOldAssetsAndFlagFridges(locationDTO);
				var data=$.map(locationDTO||[], function(obj) {
					return (showFridges || !obj.isFridge) 
						 ? {label: "<img src='"+catImgPath+listOfFields.categories[obj.asset.primaryCategory.id].icon+"'> "+obj.asset.name, value: obj.tagDTO.deviceId}
						 : null;
				});
				if (!data.length) $("#application-id").parent().addClass('is-invalid').find("b").toggle(!$.isEmptyObject(locationDTO)); // show the <b>Try showing fridges</b> message if some fridges have been hidden
				response(data);
			});	
		},
		minLength: 4,
		select: function (event, ui){
			executeFreetextSearch(ui.item.value);
		},
		//https://stackoverflow.com/questions/40782638/jquery-autocomplete-performance-going-down-with-each-search
		search: function(e,ui){
			$(this).data("ui-autocomplete").menu.bindings = $();
		},
	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
	  return $( "<li>" )
		.append( "<div><b>"+item.label+"</b><i>"+item.value+"</i></div>" ) // needs to be wrapped in a div for hover effect
		.appendTo( ul );
	};

	if (!userDepartment && !localStorage.getItem('userDepartmentAsked')) {
		deptDialog.showModal();      
	}
	localStorage.setItem('userDepartmentAsked', true);
}

function processMapsAndZones() {
	var mapsAndZones = [];

	// add maps to autocomplete list
	$.each(listOfFields.Map, function(i, obj) {
		mapsAndZones.push({label: obj.name, category:'Map', mapId: i*1, zoneId:0});
	});

	// calculate the boundaries of all the zones
	var addedBoundary = false;
	$.each(listOfFields.Zone, function(i, obj) {
		// add zones to autocomplete list
		mapsAndZones.push({label: obj.name, category:'Zone', mapId: obj.map.id*1, zoneId:i*1});
		if (!obj.boundary) {
			obj.boundary=calculateZoneBoundary(obj);
			addedBoundary=true;
		}
	});
	if (addedBoundary) localStorage.setItem('listOfFields', JSON.stringify(listOfFields));

	$('#MapZone').autocomplete({
		source: mapsAndZones,
		minLength: 1,
		//https://stackoverflow.com/questions/40782638/jquery-autocomplete-performance-going-down-with-each-search
	    search: function(e,ui){
			$(this).data("ui-autocomplete").menu.bindings = $();
		},
		select: function( event, ui ) {
			changeMap(ui.item.mapId, ui.item.zoneId, true);
		}
	});
}

function processDepartmentsAndGroups() {
	var allFields = []
	$.each(["groups", "departments"], function(n, field){
		var availableFields = $.map(listOfFields[field], function(elem, id){	 
			return {"label": elem.name, id: id*1, "category": field};
		});
		allFields = allFields.concat(availableFields);
		if(field == "departments"){
			$( "#dialog-Department" ).autocomplete({
				source: availableFields,
				select: function (event, ui){
				 	$('#dialog-Department').data("id", ui.item.id);
				},
			  	//https://stackoverflow.com/questions/40782638/jquery-autocomplete-performance-going-down-with-each-search
			  	search: function(e,ui){
					$(this).data("ui-autocomplete").menu.bindings = $();
				},
			}).val((userDepartment ? listOfFields.departments[userDepartment].name : "")).data('autocomplete');
		}
	});
	attachCategoryAutoComplete("departments", allFields);
}

function processCategories() {
	var availableFields = [];
	var baseCategories = [];

	$.each(listOfFields.categories, function (id, obj) {
		availableFields.push( {label: obj.name, id: id*1, category: "categories"} );
		if (!obj.hasChildren) baseCategories.push( {label: obj.name, id: id*1, category: "categories"} );
	});

	attachMultiAutoComplete("categories", availableFields);

	$('#filter-input').autocomplete({
		source: baseCategories,
		minLength: 1,
		//https://stackoverflow.com/questions/40782638/jquery-autocomplete-performance-going-down-with-each-search
	    search: function(e,ui){
			$(this).data("ui-autocomplete").menu.bindings = $();
		},
		select: function( event, ui ) {
			$(this).data("id", ui.item.id);
			redrawAllAssets();
		}
	});
}

/************** UI Initialising/Maintaining ******************/

function plural(n, unit) {
	return n+" "+unit+(n>1?"s":'');
}

function drawAssets(divToFill, assetsToDraw) {
	var $assetsDiv = $(divToFill);
	$assetsDiv.empty();
	if(assetsToDraw == null) return;

	var boundary=null;
	var origin={x:0, y:0};
	if ("zoneId" in assetsToDraw) { 
		boundary=listOfFields.Zone[assetsToDraw.zoneId].boundary;
	} else {
		var map = listOfFields.Map[assetsToDraw.mapId];
		boundary={minx:0, miny:-map.height*map.YScale, maxx: map.width*map.XScale, maxy: 0};
		origin = {x:map.origin.x*map.XScale, y:map.origin.y*map.YScale};
	}

	var filterShowCategoryId=$("#filter-input").data("id") || false;
	var assetPoints = $.map(assetsToDraw.assets, function(asset, assetId) {
		// ignore assets which are filtered, considered fridges or over an hour old
		// (must be in the same conditional so the numFilteredAssets is correct i.e. fridges aren't counted twice)
		var hiddenByFilter = filterShowCategoryId && (asset.asset.primaryCategory.id!=filterShowCategoryId);
		if (hiddenByFilter || (!showFridges && asset.isFridge)) return;
    	return {
    		left: 100*(asset.point.x+origin.x-boundary.minx)/(boundary.maxx-boundary.minx),
    		top: -100*(asset.point.y-origin.y-boundary.maxy)/(boundary.maxy-boundary.miny),
    		label: asset.asset.name
    			+ (asset.tempCelsius ? " ~ " + Math.round(asset.tempCelsius*10)/10+"&deg;C":'')
    			+ (asset.ageHrs>0 && asset.ageHrs<24 ? " ("+plural(asset.ageHrs, "hour")+" old)":'')
    			+ (asset.ageHrs>=24 ? " ("+plural(Math.floor(asset.ageHrs/24), "day")+" old)":''),
    		category: asset.asset.primaryCategory.id,
    		mine: (asset.asset.departments.DepartmentDTO.id == userDepartment),
    		old: asset.ageHrs>0,
    		drawn: false,
    		id: assetId
    	};
	});

    // update badges and tabs
    // need to do this even if the map hasn't loaded yet so the tabs can be sorted.
    var zoneId=divToFill.replace("#assets",'')
	if (zoneId){
		var badge = $("#badge"+zoneId);
		var tabHead = $("#tab-head"+zoneId);
		// nested if is required for the else statement
		if (assetPoints.length == 0){
			if (tabHead.hasClass("is-active")){
				//console.log("active tab is now empty");
				selectTab("find-my-assets");
			} 
			//console.log("tab hidden", tabHead)
			tabHead.hide();
			badge.hide();
		} else {
			badge.attr('data-badge', assetPoints.length);
			if(badge.css('display') == "none"){
				tabHead.show();
				badge.show();
			}
		} 
    }
    // don't bother drawing assets if the map hasn't loaded yet (css width will be 0px) since they will be redrawn once the map has loaded
    // however, need to set the tab badge count above before we return so that the tabs can be sorted.
	if ($assetsDiv.parent().css('width')=='0px') return;
	
	assetPoints.forEach(function(point){
		if(!point || point.drawn) return;		
		// check if there are overlapping assets
		var assetsTooClose = isNear(assetPoints, point, $assetsDiv.parent().css('width').replace('px','')*1, $assetsDiv.parent().css('height').replace('px','')*1);
		var assetNamesHTML = [];
		var anyMine = point.mine;		
		
		point.drawn = true;
		
		if (assetsTooClose.length > 0){
			assetsTooClose.push(point); // add current assets to the list of close assets
			$.each(assetsTooClose, function(i, assetTooClose){
				anyMine |= assetTooClose.mine;
				// add each of the close assets to the popup list of assets
				assetNamesHTML.push(
					"<div style='float:left;' class='"+(assetTooClose.mine ? "myDept": "")+(assetTooClose.old ? " old": "")+"'>"+
						"<img src='"+catImgPath+listOfFields.categories[assetTooClose.category].icon+"'>"+
						"<div class='name'>&nbsp;"+assetTooClose.label+"</div>"+
					"</div>"
				);
				assetTooClose.drawn=true; // this is modifying the original object, not a copy.
			});
		}

		$assetsDiv.append(
			"<div class='asset' style='left:"+point.left+"%;top:"+point.top+"%;'>"+
				"<div class='dot"+(anyMine ? " myDept": "")+(point.old && !assetsTooClose.length ? " old": "")+"'>"+
					(assetsTooClose.length ? "<span class='mdl-badge' data-badge='"+(assetsTooClose.length)+"'></span>" :
					"<img src='"+catImgPath+listOfFields.categories[point.category].icon+"'>")+
				"</div>"+
				"<div class='hover"+(point.left>50?' left':'')+(point.top>60?' top':'')+"'>"+
					(assetNamesHTML.length ? assetNamesHTML.join(" ") :
					"<div class='name'>"+point.label+"</div>") +
				"</div>"+
			"</div>"
		);
    });
}

function resizeMap(mapId) {
	if(typeof mapId!='string') {
		// called from the window reseize event, so need to resize all maps.
		$("div.map").each(function() {
			resizeMap(this.id.replace('map',''));
		});
		return;
	}
	var $mapImg=$("img#mapImage"+mapId);
	var headerHeight = 48+56; // fixed height to work around google JS adjustment.
	var divHeight = window.innerHeight-headerHeight;
	var screenRatio = window.innerWidth / divHeight;
	var mapRatio = $mapImg[0].naturalWidth / $mapImg[0].naturalHeight;
	var height, width;
	var top='';
	
	if (mapRatio > screenRatio) {
		// map is wider than screen so make map full width and height to suit
		width = window.innerWidth;
		height = $mapImg[0].naturalHeight * width/$mapImg[0].naturalWidth; 
		// if space allows, vertically centre map on page, otherwise, pin it to bottom of page to minimise overlap by map title
		var topMargin=124; // this is the rough height of the map title
		if (divHeight - height > 2*topMargin) top='0px';
		else if (divHeight - height > topMargin) top=topMargin+"px";
	} else {
		// map is taller than screen so make map full height and width to suit
		height = divHeight;
		width = $mapImg[0].naturalWidth * height / $mapImg[0].naturalHeight; 
	} 

	$("div#map"+mapId).css({
		'width': width+"px",
		'height': height+"px",
		'top': top
	});
	drawZones(mapId, width, height);
	// re-group close assets based on new window size.
	drawAssets("#assets"+mapId, mapId ? formattedAssets[mapId] : homeAssets);
}

function drawZones(mapId, width, height) {
	/*
	NOTE, it should be possible to create the SVG only once and make it scale automatically, using 
	vector-effect: non-scaling-stroke on the polygon to stop the line thickness growing.
	However, IE doesn't support vector-effect, so we have to redraw the SVG whenever the map is scaled!
	*/
	var $svg=$("#map"+mapId+" svg");
	$svg.empty();

	if (mapId) {
		if (mapId.indexOf('z')==0) return; //TODO need to draw zones within zones?
		mapId = mapId.replace('m','');
	} else {
		if (currentState.zoneId) return; //TODO need to draw zones within zones?
		mapId = currentState.mapId;
	}

	var boundary=null, origin=null;
	if (false) { //TODO need to draw zones within zones?
		//boundary=listOfFields.Zone[assetsToDraw.zoneId].boundary;
		//origign=??
	} else {
		var map = listOfFields.Map[mapId];
		boundary={minx:0, miny:-map.height*map.YScale, maxx: map.width*map.XScale, maxy: 0};
		origin={x: map.origin.x*map.XScale, y: -map.origin.y*map.YScale}
	}

	var x, y;
	var points, text;
	$.each(listOfFields.Zone, function(zoneId, zone) {
		if (zone.map.id != mapId) return;
		// create a text list of all the points: "x1,y1 x2,y2 x3,y3"
		points="";
		for (var p=0; p<zone.points.PointDTO.length; p++) {
			x=zone.points.PointDTO[p].x + origin.x;
			y=zone.points.PointDTO[p].y + origin.y ;
			x=width * (x-boundary.minx)/(boundary.maxx-boundary.minx);
    		y=-height * (y-boundary.maxy)/(boundary.maxy-boundary.miny);
			points += " "+x+","+y;
		}
		// put zone name in cetre of zone
		x=(zone.boundary.minx + zone.boundary.maxx)/2 + origin.x;
		y=(zone.boundary.miny + zone.boundary.maxy)/2 + origin.y;
		x=width * (x-boundary.minx)/(boundary.maxx-boundary.minx);
		y=-height * (y-boundary.maxy)/(boundary.maxy-boundary.miny);

		text=wordWrap(zone.name, 14); // split zone name into lines of ~14 chars length
		var lineHeight=15;
		y-=(lineHeight/2)*(text.length-1)+lineHeight;
		// create an array of text lines for zone name
		text=$.map(text, function(line) {
			y+=lineHeight;
			return "<text x='"+x+"' y='"+y+"' fill='black'>"+line+"</text>";
		});
		$svg.append(
			"<g onclick='zoneClick("+zoneId+")'>"+
				"<polygon points='"+points+"' fill='"+zone.color+"' stroke='"+zone.color+"'/>"+
				text.join()+
			"</g>"
		);
	});
	// force svg to be reparsed;  .parent() required for IE.
	$svg.parent().html($svg.parent().html());
}

function calculateZoneBoundary(zone) {
	var	points=zone.points.PointDTO;
	var minx=0, maxx=0, miny=0, maxy=0;
	minx=points[0].x;
	miny=points[0].y;
	maxx=points[0].x;
	maxy=points[0].y;
	for (var i=1; i<points.length; i++) {
		if (points[i].x < minx) minx=points[i].x;
		if (points[i].y < miny) miny=points[i].y;
		if (points[i].x > maxx) maxx=points[i].x;
		if (points[i].y > maxy) maxy=points[i].y;
	}
	return {minx: minx, miny: miny, maxx: maxx, maxy: maxy};
}

function selectTab(id, retryCount, fromOnPopState){
	// try to select tab for two seconds(give it time to load) then default to find-my-assets
	if(!$('a[href="#'+id+'"]').length){
		if ((retryCount||0)<10) {
			setTimeout(function() { selectTab(id, (retryCount||0)+1); }, 200);
		} else {
			selectTab("find-my-assets"); 
		}
	} else {
		// remove all is-active classes from tabs
		$('a.mdl-layout__tab').removeClass('is-active');
		// activate desired tab
		$('a[href="#'+id+'"]').addClass('is-active');
		// remove all is-active classes from panels
		$('section.mdl-layout__tab-panel').removeClass('is-active');
		// activate desired tab panel
		$('#'+id).addClass('is-active');
		// if we didn't get here from window.onpopstate, we need to record the new state
		if (!fromOnPopState) stateChange(id);
	}
}

function redrawAllAssets(){
	drawAssets("#assets", homeAssets);
	if(!formattedAssets) return;
	$.each(formattedAssets, function(id, tab){
		drawAssets("#assets"+id, tab);
	});
	sortTabs();
}

/************** Event Handling ******************/

function filterAssets(e, selectedCategoryId){
 	var id = $("#filter-input").data('id');
	if (id && ($("#filter-input").val() != listOfFields.categories[id].name)) { //if the string doesnt match the id then clear id and show all
		$("#filter-input").data('id', 0);	
		//console.log("cleared filter!");
		redrawAllAssets();
	}
}

function idSearch(){
	var applicationId = $("#application-id").val();
	var $idInput = $("#application-id");
	if(!applicationId) {
		$idInput.focus();
		return;
	}
	var idString = $.param({freeText: applicationId});
	getJSON("ajax.php?func=getAssetsFromSearchCriteria&"+ idString , function(locationDTO){
		//if result is empty incomplete id was entered
		removeOldAssetsAndFlagFridges(locationDTO);
		if (locationDTO && !$.isEmptyObject(locationDTO)){
			executeFreetextSearch(applicationId);			
		} else {
			// show error message and focus text input
			$idInput.focus().parent().addClass("is-invalid");
		}
	 });

}

function changeMap(newMapId, newZoneId, pushState) {
	// remove current assets
	homeAssets=null; // stop the old assets being redrawn if the map loads quicker than the new assets.
	drawAssets("#assets", null);

	// Update map image and assets without reloading whole page
	currentState.mapId = newMapId;
	currentState.zoneId = newZoneId;
	if(pushState) stateChange();
	if(currentState.zoneId){
		$("#home-zone-title").html("<br> &#8627;  "+listOfFields.Zone[currentState.zoneId].name);
		var map = listOfFields.Zone[currentState.zoneId].map.id;
		$("#home-map-title")
			.text(listOfFields.Map[map].name)
			.addClass("has-zone")
			.attr('data-mapid', map);
	} else {
		$("#home-zone-title").html("");
		$("#home-map-title")
			.text(listOfFields.Map[currentState.mapId].name)
			.removeClass("has-zone")
			.attr('data-mapid', '');
	}
	
	// get new assets & map
	fetchAssets();
	getMapImage();
}

function zoneClick(zoneId) {
	//if (!$("section#home.is-active").length) selectTab("home");
	changeMap(currentState.mapId, zoneId, false);
	selectTab("home");
}

function clearSearch(e){
	currentState.searchCriteria = {};
	selectTab("find-my-assets");
	
	$(".js-added-tab").remove();
	// fire a scroll event on the tab bar, to force the scroll arrows to show if too wide
	$(".mdl-layout__tab-bar").triggerNative('scroll');
	$("#clear-search-button").hide();
	$("#departments").parent("div.mdl-textfield")[0].MaterialTextfield.change("");
	$("#categories").parent("div.mdl-textfield")[0].MaterialTextfield.change("");
}

function assetSearch(e) {
	e.preventDefault();

	var err = false;
	var elements = document.getElementById("search-form").elements;
	currentState.searchCriteria = {};
	// Loop form elements
	$.each(elements, function(i, element){
		// if field not empty 
		if((element.type === "search") && (element.value !== "")){
			var inputs = element.value.split(", ").filter(function(el) {return el.length != 0}); //trims empty array at the end
			var elementIds = [];
			var $field = $("#"+element.id);
			// loop through inputs
			for(var i=0; i < inputs.length; i++){
				var fieldId = $field.data(inputs[i]+"id");
				var fieldCategory = $field.data(inputs[i]+"category");
				if(fieldId>=0 && fieldCategory && fieldId in listOfFields[fieldCategory]){
					elementIds.push(fieldId);							
				} else {
					//Invalid search move focus to first invalid field and hint to use autocomplete. leave in current state and return.
					err = true;
					if(element.id == "departments"){
						$field.catcomplete("close");
					} else {
						$field.autocomplete("close");
					}
					$field.focus().parent().addClass("is-invalid");
					return;
				}
			}
			currentState.searchCriteria[fieldCategory] = elementIds;
		} 
	});

	if(!err){
		//clear filter incase all results are filtered
		$("#filter-input")
			.data("id", 0)
			.parent("div.mdl-textfield__expandable-holder").parent("div.mdl-textfield")[0].MaterialTextfield.change('');
		$(".js-added-tab").remove();
		getAssetsBySearchCriteria(true);
	} 
}

function saveDepartment(event) {
	var departmentInput = $('#dialog-Department');
	var departmentId = departmentInput.data("id");
	// occurs if page is reloaded and a new department has not been selected (data is cleared on reload)
	if(!departmentId) departmentId = Number(localStorage.getItem("userDepartment"));
	var currentUser = $("#current-user");

	// Ensures the id associated with the input matches the input string and is not left over.
	if(departmentId in listOfFields.departments && (departmentInput.val() == listOfFields.departments[departmentId].name)){
		document.querySelector('#dept-dialog').close();
		userDepartment = departmentId;
		redrawAllAssets();
		localStorage.setItem('userDepartment', JSON.stringify(userDepartment));
		$("#set-user").text("Change User");
		currentUser.parent().show();
		var departmentName=listOfFields.departments[userDepartment].name;
		currentUser.text(departmentName);
		$("#clear-button")[0].disabled = false;

		// if no search currently, try searching for the users own assets
		if ($.isEmptyObject(currentState.searchCriteria)) {
			$("#departments")
				.data(departmentName+"id", departmentId)
				.data(departmentName+"category", 'departments')
				.parent("div.mdl-textfield")[0].MaterialTextfield.change(departmentName);
			//currentState.searchCriteria['departments'] = [userDepartment];
			//getAssetsBySearchCriteria(true);
		};
		// set the 'home' map to the user's department, if a map with a similar name exists.
		var guessMapName = departmentName.match(/(?:^\w{2,3}\s)?(.*)/)[1]; // strip "CH " from start of string (any 2-3 chars followed by space)
		$.each(listOfFields.Map, function(mapId, map) {
			if (map.name==guessMapName) {
				changeMap(mapId, map, false);
				selectTab("home");
				guessMapName=''; // so we know we succeeded
				return false;
			}
		});
		if (guessMapName) {
			// couldn't find a map with the right name, check zones...
			$.each(listOfFields.Zone, function(zoneId, zone) {
				if (zone.name==guessMapName) {
					changeMap(zone.map.id, zoneId, false);
					selectTab("home");
					return false;
				}
			});
		}
	} else {
		departmentInput.autocomplete( "close" );
		departmentInput.parent().addClass("is-invalid");
		departmentInput.focus();
	}
};


function clearDepartment(event) {
	document.querySelector('#dept-dialog').close();
	userDepartment = null;
	localStorage.removeItem("userDepartment");
	$("#set-user").text("Set User");
	$("#current-user").parent().hide();
	$("#dialog-Department").parent("div.mdl-textfield")[0].MaterialTextfield.change('');
	$("#clear-button")[0].disabled = true;
	redrawAllAssets();
}

/************** API requests ******************/

function fetchAssets(){
	getJSON("ajax.php?func=getAssetsForMap&map="+currentState.mapId+"&zone="+currentState.zoneId, function(json) {
		if(!json) return;
		// this might be the callback of a request which started before we changed map
		// check the new assets are for the correct map, and ignore if not.
		if (json.map!=currentState.mapId && json.zone!=currentState.zoneId) return;
		// create a copy of the new assets.
		removeOldAssetsAndFlagFridges(json.assets);
		homeAssets = json.zone === "0" ? {mapId: json.map, assets: JSON.parse(JSON.stringify(json.assets))} : {zoneId: json.zone, assets: JSON.parse(JSON.stringify(json.assets))};
		drawAssets("#assets", homeAssets);
	});
}

function getAssetsBySearchCriteria(selectFirstTab){
	// getAssetsBySearchCriteria({depts: [24, 16], groups: 3, zone: 1});
	if($.isEmptyObject(currentState.searchCriteria)){
		$("#clear-search-button").hide();
		$(".js-added-tab").remove();
		return;
	} 	

	// expand search string to include all child categories
	var searchCriteria=JSON.parse(JSON.stringify(currentState.searchCriteria||{})); // make a copy so don't change currentState
	searchCriteria.categories = getSelfAndDecendentCategoryIds(searchCriteria.categories||[]);
	var criteriaString = $.param(searchCriteria);

	$("#progress").show();
	getJSON("ajax.php?func=getAssetsFromSearchCriteria&"+ criteriaString , function(locationDTO){
			$("#progress").hide();
			// in case search has been cleared since the request was sent
			if ($.isEmptyObject(currentState.searchCriteria)) return;
			//console.log(locationDTO);
			removeOldAssetsAndFlagFridges(locationDTO);
			if(!locationDTO || $.isEmptyObject(locationDTO)){
				//console.log("no results!", currentState.searchCriteria);
				currentState.searchCriteria = {};
				$("#clear-search-button").hide();
				$(".js-added-tab").remove();
				$("#noResults").show();
				setTimeout(function(){ $("#noResults").hide(); }, 10000);
				return;
			} 

			// kept global so can redraw easily
			formattedAssets = formatAssetsForDrawing(locationDTO);
			// remove all assets for a tab for testing
			if (typeof removeTabId !='undefined' && removeTabId) delete formattedAssets[removeTabId];
			
			// remove tabs which no longer have assets
			$("section.js-added-tab").each(function() {
				var id=this.id.replace('tab','');
				if (!formattedAssets[id]) removeTab(id);
			});

			$("#clear-search-button").show();
			
			// If the tabs already exist empty and fill just asset div (e.g. redraw, filter, showFridges)
			$("section.js-added-tab div.asset-container").empty();
			$.each(formattedAssets, function(id, tab) {
				// create tab if it doesn't exist.
				if ($("#assets"+id).length == 0) addTab(id, tab);
				//Draw assets
				drawAssets("#assets"+id, tab);
			});

			sortTabs();

			//activate added tabs
			var layout = document.querySelector('.mdl-js-layout');
			var tabs = document.querySelectorAll('.mdl-layout__tab');
			var panels = document.querySelectorAll('.mdl-layout__tab-panel');
			for (var i = 0; i < tabs.length; i++) {
				new MaterialLayoutTab(tabs[i], tabs, panels, layout.MaterialLayout);
			}	

			// Once tabs are loaded - fire a scroll event on the tab bar, to force the scroll arrows to show if too wide
			$(".mdl-layout__tab-bar").triggerNative('scroll');
			
			// select the first search tab if this function has been called from the search click listener
			if(selectFirstTab){
				var firstTab=$("#tabBar .js-added-tab:visible")[0];
				if (firstTab) {
					firstTab.click();
				} else {
					selectTab("find-my-assets");
					$("#noResults").show();
					setTimeout(function(){ $("#noResults").hide(); }, 10000);
				}
			}
	 });
}

function addTab(id, zone){
	$("#tabBar").append(
		"<a id='tab-head"+ id +"' href='#tab"+ id +"' class='mdl-layout__tab js-added-tab'>"+
			zone.name+
			"<span id='badge"+ id +"' class='mdl-badge' data-badge="+ (showFridges ? zone.assets.length : (zone.assets.length - zone.fridgeCount))+"></span>"+
		"</a>" 
	);
	var parentMapId = ("zoneId" in zone) && listOfFields.Zone[zone.zoneId].map.id;
	
	$("#main").append(
		"<section class='mdl-layout__tab-panel js-added-tab' id='tab"+ id +"'>"+
			"<div>"+
				"<div id='title"+id+"' class='tab-title mdl-color-text--grey-600'>"+
						"<div>"+
							(parentMapId ? "<span id='parentMap"+zone.zoneId+"' class='tab-title-map mdl-typography--display-1 has-zone' data-mapid='"+parentMapId+"'>"+listOfFields.Map[parentMapId].name+"<br>  &#8627;  "+"</span>":"")+
							"<span class='tab-title-zone mdl-typography--display-1'>"+zone.name+"</span>"+
						"</div>"+
						(zone.extension ? "<p class='extension'>Extension: "+zone.extension+"</p>" : '')+
				"</div>"+
				"<div class='map' id='map"+ id +"'>"+
						"<img src='' class='tabmap map' id='mapImage"+ id +"'/>"+
						"<svg/>"+
						"<div id='assets"+ id +"' class='asset-container'/>"+
				"</div>"+
			"</div>"+
		"</section>"
	);

	getMapImage(id);
}

function getMapImage(id){
	id=id || '';
	var img = id ? id.replace('m','map_').replace('z','zone_')
				 : (currentState.zoneId ? 'zone_'+currentState.zoneId : 'map_'+currentState.mapId);
	$("#map"+id).hide();
	$("#mapImage"+id).attr("src", 'cache/'+img+'.v'+listOfFieldsDate+'.jpg').one('load', function() {
		$("#map"+id).show();
		resizeMap(id);
	});
}

function removeTab(id){
	var tabHead = $('#tab-head'+id);
	if (tabHead.hasClass("is-active")){
		 selectTab("find-my-assets");
	}
	if(tabHead.length){
		tabHead.remove();
		$('#badge'+id+',#tab'+id).remove();
	}
}

function sortTabs() {
	// sort tabs based on new asset counts
	var $tabBar=$("#tabBar");
	var $tabs=$tabBar.children(".js-added-tab");
	var beforeOrder=$tabs.map(function(a) {return this.id}).get().join();
	$tabs.sort(function(a,b) {
		var aCount=$(a).find('.mdl-badge').attr('data-badge')*1;
		var bCount=$(b).find('.mdl-badge').attr('data-badge')*1;
		return bCount==aCount ? a.text.localeCompare(b.text) : bCount-aCount;
	});
	var afterOrder=$tabs.map(function(a) {return this.id}).get().join();
	if (beforeOrder!=afterOrder) $tabs.detach().appendTo($tabBar); // only move tabs if necessary to avoid flicker
}

/************** Navigation Handling ******************/

function stateChange(newHash) {
	// Add currentState and hashstate to history
	var criteriaString = currentState.searchCriteria && $.param(currentState.searchCriteria);
	var queryString = "?map="+currentState.mapId+"&zone="+currentState.zoneId+"&searchCriteria="+criteriaString;
	var pageName = JSON.stringify(currentState);
	var hash = newHash || window.location.hash.substr(1);
	history.pushState(currentState, pageName, queryString+"#"+hash);
	localStorage.setItem('queryString', queryString+"#"+hash);
	//console.warn('state Change', newHash, window.location.hash.substr(1));
	//console.log(pageName);

	var pageTitle = (hash=="home" && (currentState.zoneId ? listOfFields.Zone[currentState.zoneId].name : listOfFields.Map[currentState.mapId].name))
				 || (hash=="find-my-assets" && "Find My Assets")
				 || $("#"+hash.replace('tab','tab-head')).text();
	$("title").text("Asset Tracker"+(pageTitle?' - '+pageTitle:''));
}

window.onpopstate = function(event) {
 	event.preventDefault();
 	localStorage.setItem('queryString', window.location.search+window.location.hash);

 	drawAssets("#assets", null);

	var oldSearchCriteria = JSON.stringify(currentState.searchCriteria);
	currentState = getCurrentStateFromURL();
	if (JSON.stringify(currentState.searchCriteria) != oldSearchCriteria) {
		populateSearchFields(false); // this also clears tabs and runs the search
	}

	var hash = window.location.hash.substr(1);
	changeMap(currentState.mapId, currentState.zoneId, false);  // this also initiates the fetchAssets and getAssetsBySearchCriteria
	if (hash && !($("#"+hash+".is-active").length)) selectTab(hash, 0 /* retryCount*/, true /* fromOnPopState */);
};

function hashChange(){
	// called when user clicks on a tab.  MDL will handle the tab display automatically, we just need to record the state change
	// window.location.hash will still be old hash.  we need to get new has from this.href (this=<a> link)
	var hash;
	//console.log('hashChange', window.location.hash, window.location.href);
	try{
		hash = this.href.substring(this.href.indexOf('#')+1);
		if (window.location.hash=="#"+hash) return;
	} catch(e) {
		//console.log("Unrecognized hash state! Navigating to home..");
		selectTab("home");
		hash = "home";
	}
	//console.log('hash change', hash);
	stateChange(hash);
}

/************** Helper Functions ******************/

function populateSearchFields(selectFirstTab) {
	if(currentState.searchCriteria && !currentState.searchCriteria.freeText){
		$.each(currentState.searchCriteria, function(field, id){				
			if(field == "categories"){
				var val = "";
				$.each(id, function(i, catId){
					val = val.concat(listOfFields[field][catId].name+", ");
					$("#"+field)
						.data(listOfFields[field][catId].name+"id", catId)
						.data(listOfFields[field][catId].name+"category", field);
				});
				$("#categories").parent("div.mdl-textfield")[0].MaterialTextfield.change(val);
			} else {	
				var val = listOfFields[field][id].name;
				$("#departments")
					.data(val+"id", id[0])
					.data(val+"category", field)
					.parent("div.mdl-textfield")[0].MaterialTextfield.change(val);
			}		
		});
		$(".js-added-tab").remove();
	}
	getAssetsBySearchCriteria(selectFirstTab);
}

function executeFreetextSearch(freetext){
	// update searchCriteria and execute search.
	currentState.searchCriteria = {};
	currentState.searchCriteria["freeText"] = freetext;
	getAssetsBySearchCriteria(true);
	// close dialog.
	$("#id-search-dialog")[0].close();
}
	
function wordWrap(text, width){
    var lines=text.split(" ");
    var joined=[];
    var line=''
    for (var i in lines) {
      if (line=='' || (line+lines[i]).length<width) {
        line+=(line?' ':'')+lines[i];
      } else {
        joined.push(line);
        line=lines[i];
      }
    }
    if (line) joined.push(line);
    return joined;
}

function isNear(assetPoints, assetPoint, width, height) {
	var margin=2400; // pixels*100, .left and .top are % of div width, so actual proximity will depend on div width and height
	return assetPoints.filter(function(element){
		// element doesn't exist or is already drawn or is the one we are comparing to then return.
		if (!element || element.drawn || element.id==assetPoint.id) return false;
		return (Math.abs(element.left - assetPoint.left)*width < margin && Math.abs(element.top - assetPoint.top)*height < margin);
	});
}

function getSelfAndDecendentCategoryIds(categoryId){
	// if passed an array of categories, call this function for each category in the array.
	if (typeof categoryId=='object') { // arrays are objects
		return $.map(categoryId, function(id) {
			return getSelfAndDecendentCategoryIds(id);
		});
	}
	// if called with a single category, add id to list and then recurse for immediate children
	// each child id will automatically be added to the list as part of the next recursion.
	return [Number(categoryId)].concat($.map(listOfFields.categories[categoryId].children, function(child) {
		return getSelfAndDecendentCategoryIds(child.id);
	}));
}

function getCurrentStateFromURL() {
	var zoneId = getQueryElement("zone");
	var mapId = getQueryElement("map");
	var zoneKeys = Object.keys(listOfFields.Zone);
	var mapKeys = Object.keys(listOfFields.Map);
	zoneId=(zoneId && zoneKeys.includes(zoneId)) ? zoneId : 0; // if no zone set to zero so the map is loaded
	return {
		mapId: zoneId
				? listOfFields.Zone[zoneId].map.id  // if a zone is set the use the correct map, in case a zone gets moved
				: ((mapId && mapKeys.includes(mapId)) ? Number(mapId) : mapKeys[3]) , //zone ? listOfFields.Zone[zone].map.id : () ,
		zoneId: Number(zoneId), 
		searchCriteria: getSearchCriteria()
	};
}

function getQueryElement(elem, regex) {
	var regEx = regex || new RegExp("[?&]"+elem+"=([^#&?]*)","i");
	var a = location.search.match(regEx);
	return a && a[1];
}

function getSearchCriteria(search) {
	//only return criteria if it is a valid search (prevents API returning all assets for a null search)
	var search = getQueryElement("searchCriteria", new RegExp("[&]searchCriteria=([^#]*)", "i"));

	if($.isEmptyObject(search) || search == "null") return {};
	var criteria = $.deparam(search);
	return (criteria.groups || criteria.categories || criteria.departments || criteria.freeText)
		? criteria
		: {};
}

function removeOldAssetsAndFlagFridges(assets) {
	var now=new Date();
	if (temperatureMonitoringCategories.length==1) temperatureMonitoringCategories=getSelfAndDecendentCategoryIds(temperatureMonitoringCategories);
	assets && $.each(assets, function(id, asset) {
		asset.ageHrs = Math.floor((now - new Date(asset.dateCreated)) / (60*60*1000));
		if (asset.ageHrs && !showOld)
			delete assets[id];
		else if (temperatureMonitoringCategories.includes(asset.asset.primaryCategory.id))
			asset.isFridge=true;
	});
}

function formatAssetsForDrawing(assetsToFormat){
	// group assets by zone or map
	var zones = {};
	$.each(assetsToFormat, function(i, asset){
		if (asset.zones) {
			var zone =  asset.zones.ZoneDTO;
			if (zone.constructor === Array) zone= zone[0];

			if (!zones["z"+zone.id]) {
				var ext = (listOfFields.Zone[zone.id].description||'').match(/:\s*(\d*)/);
				ext=ext && ext[1];
				zones["z"+zone.id]={zoneId: zone.id, fridgeCount: 0, name: listOfFields.Zone[zone.id].name, extension: ext, assets: {}}; 
			}
			zones["z"+zone.id].assets[i]=asset;
			if (asset.isFridge) zones["z"+zone.id].fridgeCount++;
		} else {
			if (!zones["m"+asset.map.id]) zones["m"+asset.map.id]={mapId: asset.map.id, fridgeCount: 0, name: listOfFields.Map[asset.map.id].name, assets: {}};
			zones["m"+asset.map.id].assets[i]=asset;
			if (asset.isFridge) zones["m"+asset.map.id].fridgeCount++;
		}
	});
	
	return zones;
}